FROM node:14.17.3

WORKDIR /node/project

COPY ["package.json", "package-lock.json", "/"]

RUN npm install

COPY . .

ENTRYPOINT [ "npm", "start" ]

CMD [ "npm", "start" ]